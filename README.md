# fix-puppet-manifest

*fix-puppet-manifest* is used to check and fix [Puppet manifests](https://puppet.com/docs/puppet/latest/puppet_language.html).

The checks that are performed are not incompatible with the [Puppet language style guide](https://puppet.com/docs/puppet/latest/style_guide.html#the_puppet_language_style_guide): they complete it.

*fix-puppet-manifest* is not intended to replace [Puppet-lint](http://puppet-lint.com/): it completes it. Both are compatible, and the execution of both script is idempotent.

## Checks

* Braces (`{}`) style used to declare [classes](https://puppet.com/docs/puppet/latest/lang_classes.html): [Allman](https://en.wikipedia.org/wiki/Indentation_style#Allman_style) or [K&R](https://en.wikipedia.org/wiki/Indentation_style#K&R_style)
* Braces style used to declare [resources](https://puppet.com/docs/puppet/latest/lang_resources.html): Allman or K&R
* Allignment of what follows arrows (`=>`)

## Usage

*fix-puppet-manifest* can be used as a standalone script, or as a [pre-commit](https://pre-commit.com) hook.

### As a standalone script

Here is a sample manifest, written in the `init.pp` file:

```
class foo
{
  file { '/directory/file':
    owner => root,
    group =>  root,
  }
}
```

You can launch *fix-puppet-manifest* without any option but the manifest you want to check:

```
$ fix-puppet-manifest init.pp
init.pp : 1 items had been declared using Allman instead of K and R
init.pp : 1 "=>" indentation errors found
```

This is the "read-only" mode: your manifest will remain unchanged. If you want to fix the manifest, use the `--fix` option:

```
$ fix-puppet-manifest --fix init.pp
init.pp : 1 items had been declared using Allman instead of K and R
init.pp : 1 "=>" indentation errors found
init.pp : Fixed
$ cat init.pp
class foo {
  file { '/directory/file':
    owner => root,
    group => root,
  }
}
```

Launch `fix-puppet-manifest --help` to get the full options list.

### As a *pre-commit* hook

To use *fix-puppet-manifest* as a [pre-commit](https://pre-commit.com) hook, set this in your `.pre-commit-config.yaml` file:

```
repos:
-   repo: https://gitlab.com/mdavranche/fix-manifest
    rev: master
    hooks:
    -   id: fix-puppet-manifest
```

The options listed with `fix-puppet-manifest --help` are also available for *pre-commit* hook. For instance:

```
repos:
-   repo: https://gitlab.com/mdavranche/fix-manifest
    rev: master
    hooks:
    -   id: fix-puppet-manifest
        args:
        -    --classes=Allman
        -    --resources=Allman
        -    --fix
```

## FAQ

**Should I use Allman or K&R style?**

The Puppet language style guide do not impose or recommend any of them. All seems to be allowed.

To fit with what we can see in the examples of the style guide, the K&R style is the one that is used. So this is the one we have chosen K&R as the default, for classes and resources declarations. You can override this choose using the `--classes` and `--resources` options.

**Why do those checks are not performed by Puppet-lint?**

They should be. If you want those checks to be fully standard, [write a Puppet-lint check](http://puppet-lint.com/developer/tutorial/), publish it, then tell me, and I'll remove this code and Git repository.
