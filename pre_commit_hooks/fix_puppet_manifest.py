#!/usr/bin/python3

# Copyright (C) 2020 Mikael Davranche

# fix-manifist is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with erine.mail.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import re


def check_kandr_style(manifest_content, item, fix):
    """Check the K and R style of the item's braces

    Returned tuple:
    - Number of items of the Puppet manifest that were not declared using KandR
      style
    - The content, that should have been modified
    """

    # pattern is pattern_init followed by the resource name itself, set in group
    # 3, a "{" that is not on the same line.
    #
    # As we want to match the most spaces possible before "{", we make the
    # previous regex non-greedy with the "?".
    pattern = pattern_init + '(' + item + r'?)(?:\s*\n\s*{)'

    occurencies = len(re.findall(pattern, manifest_content))
    if occurencies > 0 and fix:
        manifest_content = re.sub(pattern, "\\1\\2\\3 {", manifest_content)
    return(occurencies, manifest_content)


def check_allman_style(manifest_content, item, fix):
    """Check the Allman style of the item's braces

    Returned tuple:
    - Number of items of the Puppet manifest that were not declared using Allman
      style
    - The content, that should have been modified
    """

    # pattern is pattern_init followed by the class definition itself, set in
    # group 3, and a "{" on the same line.
    #
    # As we want to match the most spaces possible before "{", we make the
    # previous regex non-greedy with the "?".
    pattern = pattern_init + '(' + item + '?)[ \t]*{'

    occurencies = len(re.findall(pattern, manifest_content))
    if occurencies > 0 and fix:
        manifest_content = re.sub(pattern, "\\1\\2\\3\n\\2{", manifest_content)
    return(occurencies, manifest_content)


def check_arrows(manifest_content, fix):
    """Check if "=>" are followed by a single space

    Returned tuple:
    - Number of => of the Puppet manifest that were not followed by a single
      space
    - The content, that should have been modified
    """

    # pattern is a => followed by no space
    pattern = '(?:=>[^ \t])'

    # Or a => followed by more than 1 space
    pattern += '|(?:=>[ \t][ \t]+)'

    occurencies = len(re.findall(pattern, manifest_content))
    if occurencies > 0 and fix:
        manifest_content = re.sub(pattern, "=> ", manifest_content)
    return(occurencies, manifest_content)


def check_manifest(manifest_file, resources_style, classes_style, fix):
    """Read manifest_file and lanch all the checks

    Return True if all the check returned no error
    """

    # Read manifest
    try:
        fd = open(manifest_file, 'r')
    except Exception as e:
        print(e)
        exit(2)
    content = "".join(fd.readlines())
    fd.close()

    # Check manifest, fix if needed and asked
    kandr_errors = 0
    allman_errors = 0
    if resources_style == 'KandR':
        for resourcetype in resourcetypes:
            (errors, content) = check_kandr_style(content, resourcetype, fix)
            kandr_errors += errors
    elif resources_style == 'Allman':
        for resourcetype in resourcetypes:
            (errors, content) = check_allman_style(content, resourcetype, fix)
            allman_errors += errors
    else:
        exit(2)
    if classes_style == 'KandR':
        (errors, content) = check_kandr_style(content, 'class[^{\n]+', fix)
        kandr_errors += errors
    elif classes_style == 'Allman':
        (errors, content) = check_allman_style(content, 'class[^{\n]+', fix)
        allman_errors += errors
    else:
        exit(2)
    (arrow_errors, content) = check_arrows(content, fix)
    if kandr_errors:
        print(manifest_file, ":", kandr_errors, "items had been declared using Allman instead of K and R")
    if allman_errors:
        print(manifest_file, ":", allman_errors, "items had been declared using K and R instead of Allman")
    if arrow_errors:
        print(manifest_file, ":", arrow_errors, "\"=>\" indentation errors found")
    if (kandr_errors or allman_errors or arrow_errors):
        if fix:
            try:
                fd = open(manifest_file, 'w')
            except Exception as e:
                print(e)
                exit(2)
            fd.write(content)
            fd.close()
            print(manifest_file, ": Fixed")
        return False
    return True


# Define pattern_init, the common part between KandR and Allman patterns
#
# It first starts with the beginning of the file or the beginning of the line,
# set in group 1.
pattern_init = '(^|\n)'

# Eventually, it can be followed by an indent, set in group 2.
#
# We do not use \s, as we don't want to match \n nor \r.
#
# \n is the same as [ \t\n\r\f\v], but \f and \v make no sense in Python, so we
# only keep [ \t].
pattern_init += '([ \t]*)'

# Get options
parser = argparse.ArgumentParser(description="Parses Puppet manifest, looking for braces styles and indentation errors")
parser.add_argument("manifest", nargs="+", help="Puppet manifest to parse")
parser.add_argument("--fix", action="store_true", help="Fix the Puppet manifest")
parser.add_argument("--classes", action="store", help="Force braces style for classes", default="KandR", choices=["Allman", "KandR"])
parser.add_argument("--resources", action="store", help="Force braces style for resources", default="KandR", choices=["Allman", "KandR"])
args = parser.parse_args()

# Puppet resource types
resourcetypes = {
    "augeas",
    "computer",
    "cron",
    "exec",
    "file",
    "filebucket",
    "group",
    "host",
    "interface",
    "k5login",
    "macauthorization",
    "mailalias",
    "maillist",
    "mcx",
    "mount",
    "nagios_command",
    "nagios_contact",
    "nagios_contactgroup",
    "nagios_host",
    "nagios_hostdependency",
    "nagios_hostescalation",
    "nagios_hostextinfo",
    "nagios_hostgroup",
    "nagios_service",
    "nagios_servicedependency",
    "nagios_serviceescalation",
    "nagios_serviceextinfo",
    "nagios_servicegroup",
    "nagios_timeperiod",
    "notify",
    "package",
    "resources",
    "router",
    "schedule",
    "scheduled_task",
    "selboolean",
    "selmodule",
    "service",
    "ssh_authorized_key",
    "sshkey",
    "stage",
    "tidy",
    "user",
    "vlan",
    "yumrepo",
    "zfs",
    "zone",
    "zpool",
}

# Launch tests and fixes
all_good = True
for manifest in args.manifest:
    all_good &= check_manifest(manifest, args.resources, args.classes, args.fix)
exit(0 if all_good else 1)
